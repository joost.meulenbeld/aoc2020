import re
from pathlib import Path
from typing import List


def parse_rule_dictionary(lines: List[str]) -> dict:
    """Get a dictionary of the rules. Maps integer to either:
        - a string
        - a list of tuples, each containing integers."""
    rules = dict()
    for line in lines:
        rule_number, rule = line.split(': ')
        rule_number = int(rule_number)
        if rule == '"a"' or rule == '"b"':
            rules[rule_number] = rule[1]
        else:
            parts = rule.split(" | ")
            rules[rule_number] = [tuple(int(i) for i in part.split()) for part in parts]
    return rules


def build_regex(rule_dictionary: dict, start_rule: int = 0, rule_8_countdown: int = 0,
                rule_11_countdown: int = 0) -> str:
    """Solution for part 2: just do the infinite loop rule_8_countdown times deep and it will work if high ennough :)"""
    rule = rule_dictionary[start_rule]
    if isinstance(rule, str):
        return rule
    rule_strings = list()
    if start_rule == 8 and rule_8_countdown > 0:
        rule = [(42,), (42, 8)]
        rule_8_countdown -= 1
    if start_rule == 11 and rule_11_countdown > 0:
        rule = [(42, 31), (42, 11, 31)]
        rule_11_countdown -= 1
    for rule_list in rule:
        rule_strings.append(''.join(build_regex(rule_dictionary, rule_number, rule_8_countdown, rule_11_countdown) for rule_number in rule_list))
    pattern = f"({'|'.join(rule_strings)})"
    if start_rule == 0:
        pattern = f"^{pattern}$"
    return pattern


if __name__ == '__main__':
    text = Path("input_files", "day19.txt").read_text().strip()
    text_rules, text_messages = text.split("\n\n")
    rule_dictionary = parse_rule_dictionary(text_rules.strip().splitlines())
    rule_pattern_part_one = re.compile(build_regex(rule_dictionary))
    print(f"Part one: {sum(rule_pattern_part_one.match(message) is not None for message in text_messages.strip().splitlines())}")
    rule_pattern_part_two = re.compile(build_regex(rule_dictionary, rule_8_countdown=100, rule_11_countdown=100))
    print(f"Part two: {sum(rule_pattern_part_two.match(message) is not None for message in text_messages.strip().splitlines())}")
