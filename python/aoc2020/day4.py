import re
from pathlib import Path
from typing import List, Dict


def parse_text(text: str) -> List[Dict[str, str]]:
    passports_texts = text.split("\n\n")
    passports_texts = [p.replace("\n", " ").split() for p in passports_texts]
    return [dict(element.split(":") for element in passport_text) for passport_text in passports_texts]


def passport_is_valid_part_one(passport: dict) -> bool:
    return len(passport) == 8 or (len(passport) == 7 and "cid" not in passport)


def passport_is_valid_part_two(passport: dict) -> bool:
    if not passport.keys() >= {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}:
        return False
    byr_valid = 1920 <= int(passport["byr"]) <= 2002
    iyr_valid = 2010 <= int(passport["iyr"]) <= 2020
    eyr_valid = 2020 <= int(passport["eyr"]) <= 2030

    if passport["hgt"].endswith("cm"):
        hgt_valid = 150 <= int(passport["hgt"][:-2]) <= 193
    elif passport["hgt"].endswith("in"):
        hgt_valid = 59 <= int(passport["hgt"][:-2]) <= 76
    else:
        hgt_valid = False

    hcl_valid = re.match(r"^#[\da-f]{6}$", passport["hcl"]) is not None
    ecl_valid = passport["ecl"] in {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}
    pid_valid = re.match(r'^\d{9}$', passport["pid"]) is not None
    return byr_valid and iyr_valid and eyr_valid and hgt_valid and hcl_valid and ecl_valid and pid_valid


if __name__ == '__main__':
    text = Path("input_files", "day4.txt").read_text()
    passports = parse_text(text)
    print(f"Part one: {sum(passport_is_valid_part_one(p) for p in passports)}")
    print(f"Part two: {sum(passport_is_valid_part_two(p) for p in passports)}")
