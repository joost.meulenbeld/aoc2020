from collections import Counter, defaultdict
from pathlib import Path
from typing import List


def get_number_of_differences(numbers: List[int]):
    numbers = sorted(numbers)
    differences = [i2 - i1 for i1, i2 in zip(numbers, numbers[1:])]
    return Counter(differences)


def get_number_of_arrangements(numbers: List[int]):
    numbers = sorted(numbers)
    paths_from_start = defaultdict(int)
    paths_from_start[0] = 1
    for number in numbers[1:]:
        paths_from_start[number] += paths_from_start[number - 1] + paths_from_start[number - 2] + paths_from_start[
            number - 3]
    return paths_from_start[numbers[-1]]


if __name__ == '__main__':
    text = Path("input_files", "day10.txt").read_text().strip()
    numbers = list(map(int, text.splitlines()))
    numbers = [0, *numbers, max(numbers) + 3]
    c = get_number_of_differences(numbers)
    print(f"Part one: {c[1] * c[3]}")
    print(f"Part two: {get_number_of_arrangements(numbers)}")
