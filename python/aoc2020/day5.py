from pathlib import Path


def get_id_from_boarding_pass(boarding_pass: str) -> int:
    binary = "".join("1" if letter in "BR" else "0" for letter in boarding_pass)
    return int(binary, base=2)


if __name__ == '__main__':
    boarding_passes = Path("input_files", "day5.txt").read_text().strip().splitlines()
    ids = sorted(get_id_from_boarding_pass(bp) for bp in boarding_passes)
    print(f"Part 1: {ids[-1]}")

    my_id = next(id1 + 1 for id1, id2 in zip(ids, ids[1:]) if id2 - id1 == 2)
    print(f"Part 2: {my_id}")
