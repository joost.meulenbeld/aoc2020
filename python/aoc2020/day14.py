import itertools
import re
from collections import defaultdict
from pathlib import Path
from typing import List


def mask_value(original_value: int, mask: str) -> int:
    binary = f"{original_value:036b}"
    masked = ''.join(value if masker == "X" else masker for value, masker in zip(binary, mask))
    return int(masked, base=2)


def mask_memory_address(original_address: int, mask: str) -> str:
    binary = f"{original_address:036b}"
    return ''.join(value if masker == "0" else masker for value, masker in zip(binary, mask))


def run_masking_computer(instructions: List[str]):
    memory = defaultdict(int)
    instruction_parser = re.compile(r"mem\[(\d+)] = (\d+)")
    mask = None
    for instruction in instructions:
        if instruction.startswith("mask"):
            mask = instruction[7:]
        else:
            location, value = instruction_parser.findall(instruction)[0]
            masked_value = mask_value(int(value), mask)
            memory[int(location)] = masked_value
    return memory


def run_masking_computer_v2(instructions: List[str]):
    """This can probably be done more efficiently instead of adding all permutations."""
    memory = defaultdict(int)
    instruction_parser = re.compile(r"mem\[(\d+)] = (\d+)")
    mask = None
    for instruction in instructions:
        if instruction.startswith("mask"):
            mask = instruction[7:]
        else:
            location, value = instruction_parser.findall(instruction)[0]
            masked_memory = mask_memory_address(int(location), mask)
            n_x = sum(l == "X" for l in mask)
            for permutation in itertools.product(("0", "1"), repeat=n_x):
                permutation_iterator = iter(permutation)
                address = ''.join(l if l != "X" else next(permutation_iterator) for l in masked_memory)
                memory[address] = int(value)
    return memory


if __name__ == '__main__':
    text = Path("input_files", "day14.txt").read_text()
    lines = text.strip().splitlines()
    memory = run_masking_computer(lines)
    print(f"Part one: {sum(memory.values())}")
    print(f"Part two: {sum(run_masking_computer_v2(lines).values())}")
