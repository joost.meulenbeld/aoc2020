from pathlib import Path

import numpy as np


def load_grid_from_text(text: str) -> np.ndarray:
    """Return a numpy array with 1 as # and 0 as ., dtype=np.uint8"""
    lines = text.strip().splitlines()
    arr = np.array([list(l) for l in lines])
    return (arr == "#").astype(np.uint8)


def get_number_of_trees(array: np.ndarray, right: int, down: int) -> int:
    """Return the number of encountered trees for the given rational path."""
    total = 0
    for i, row in enumerate(range(0, array.shape[0], down)):
        column = (right * i) % array.shape[1]
        total += array[row, column]
    return total


if __name__ == '__main__':
    text = Path("input_files", "day3.txt").read_text()
    array = load_grid_from_text(text)

    print(f"Part 1: {get_number_of_trees(array, 3, 1)}")

    product = 1
    for right, down in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]:
        product *= get_number_of_trees(array, right, down)
    print(f"Part 2: {product}")
