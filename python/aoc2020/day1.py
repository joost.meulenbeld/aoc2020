from pathlib import Path
from typing import List, Optional, Tuple


def contains_sum_of_2_numbers(numbers: List[int], total: int) -> Optional[Tuple[int, int]]:
    numbers_set = set(numbers)
    for number in numbers_set:
        if (remainder := total - number) in numbers_set and remainder != number:
            return number, remainder
    return None


if __name__ == '__main__':
    """Lazy solution for day 1. Won't work for large input sizes."""

    with open(Path("input_files", "day1.txt")) as f:
        integers = [int(line) for line in f.readlines()]

    contains = contains_sum_of_2_numbers(integers, 2020)
    print(contains)
    print(contains[0] * contains[1])

    for i in integers:

        for j in integers:
            for k in integers:
                if i + j + k == 2020:
                    print(i * j * k)
