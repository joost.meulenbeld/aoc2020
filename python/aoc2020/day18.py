import re
from pathlib import Path
from typing import Callable


def reduce_without_parentheses_add_first(expression: str) -> str:
    parts = expression.split()
    i = 0
    while i < len(parts) - 2:
        if parts[i + 1] == "+":
            result = str(int(parts[i]) + int(parts[i + 2]))
            parts = [*parts[:i], result, *parts[i + 3:]]
        else:
            i += 2
    result = int(parts[0])
    for operator, value in zip(parts[1::2], parts[2::2]):
        if operator == "*":
            result *= int(value)
        else:
            raise ValueError(f"Unkown operator {operator}")
    return str(result)


def reduce_without_parentheses_no_precedence(expression: str) -> str:
    parts = expression.split()
    result = int(parts[0])
    for operator, value in zip(parts[1::2], parts[2::2]):
        if operator == "*":
            result *= int(value)
        elif operator == "+":
            result += int(value)
        else:
            raise ValueError(f"Unkown operator {operator}")
    return str(result)


def evaluate_expression(expression: str, reduction: Callable) -> int:
    while True:
        prev_length = len(expression)
        expression = re.sub(r"\(([^()]+)\)", lambda match: reduction(match.group(1)), expression)
        if prev_length == len(expression):
            break
    return int(reduction(expression))


if __name__ == '__main__':
    text = Path("input_files", "day18.txt").read_text().strip()
    lines = text.splitlines()
    print(f"Part 1: {sum(evaluate_expression(line, reduce_without_parentheses_no_precedence) for line in lines)}")
    print(f"Part 2: {sum(evaluate_expression(line, reduce_without_parentheses_add_first) for line in lines)}")
