from pathlib import Path
from typing import List, Set, Tuple


def run_computer(instructions: List[Tuple[str, int]]):
    pointer = 0
    accumulator = 0
    finished = False
    visited: Set[int] = set()
    while pointer not in visited:
        visited.add(pointer)
        instruction, amount = instructions[pointer]
        if instruction == "acc":
            accumulator += amount
            pointer += 1
        elif instruction == "jmp":
            pointer += amount
        elif instruction == "nop":
            pointer += 1
        if pointer == len(instructions):
            finished = True
            break
    return accumulator, finished


def fix_corruption(original_instructions: List[Tuple[str, int]]):
    nop_jmp_pointers = [i for i, (instr, _) in enumerate(original_instructions) if instr in ("jmp", "nop")]
    for i in nop_jmp_pointers:
        new_instructions = list(original_instructions)
        instruction, amount = original_instructions[i]
        new_instructions[i] = ({"nop": "jmp", "jmp": "nop"}[instruction], amount)
        accumulator, finished = run_computer(new_instructions)
        if finished:
            return accumulator


if __name__ == '__main__':
    text = Path("input_files", "day8.txt").read_text()
    lines = [l.split() for l in text.strip().splitlines()]
    instructions = [(instr, int(amount)) for instr, amount in lines]
    print(f"Part 1: {run_computer(instructions)}")
    print(f"Part 1: {fix_corruption(instructions)}")
