from pathlib import Path
from typing import List

from aoc2020.day1 import contains_sum_of_2_numbers


def get_first_invalid_number(numbers: List[int], preamble_size=25) -> int:
    for i, number in enumerate(numbers[preamble_size:], start=preamble_size):
        if contains_sum_of_2_numbers(numbers[i - preamble_size:i], number) is None:
            return number


def get_encryption_weakness(numbers, invalid_number):
    i_start = 0
    i_end = 0
    running_sum = numbers[0]
    while running_sum != invalid_number:
        if running_sum < invalid_number:
            i_end += 1
            running_sum += numbers[i_end]
        elif running_sum > invalid_number:
            running_sum -= numbers[i_start]
            i_start += 1

    return min(numbers[i_start:i_end]) + max(numbers[i_start:i_end])


if __name__ == '__main__':
    text = Path("input_files", "day9.txt").read_text().strip()
    preamble_size = 25
    numbers = list(map(int, text.splitlines()))

    invalid_number = get_first_invalid_number(numbers, preamble_size=preamble_size)
    print(f"Part one: {invalid_number}")
    print(f"Part two: {get_encryption_weakness(numbers, invalid_number)}")
