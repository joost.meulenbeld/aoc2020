from pathlib import Path
from typing import Set, List


def do_1(text: str) -> List[Set[str]]:
    return [set(group.replace("\n", "")) for group in text.split("\n\n")]


def do_2(text: str) -> List[Set[str]]:
    groups = [group.splitlines() for group in text.split("\n\n")]
    return [set.intersection(*(set(person) for person in group)) for group in groups]


if __name__ == '__main__':
    text = Path("input_files", "day6.txt").read_text().strip()
    print(f"Part one: {sum(len(answers) for answers in do_1(text))}")
    print(f"Part two: {sum(len(answers) for answers in do_2(text))}")

