from collections import defaultdict
from pathlib import Path
from typing import List, Dict


def get_contains_dict(lines: List[str]) -> Dict[str, dict]:
    """Return a dict that maps a color to a dict"""
    contains_dict = dict()
    for line in lines:
        container, contained = line.split(" contain ")
        container = container[:-5]
        contained = [c.replace(" bags", "").replace(" bag", "").split(' ', maxsplit=1) for c in
                     contained[:-1].split(", ")]
        contained_with_amount = {color: int(amount) for amount, color in contained if amount != "no"}
        assert container not in contains_dict
        contains_dict[container] = contained_with_amount
    return contains_dict


def can_contain_color_recursive(search_name: str, contains_dict: dict) -> int:
    checked = set()
    to_check = [search_name]
    inverse_tree = defaultdict(list)

    for container, contained in contains_dict.items():
        for el in contained:
            inverse_tree[el].append(container)

    total = 0
    print(inverse_tree)
    while to_check:
        check = to_check.pop()
        checked.add(check)
        total += 1
        for el in inverse_tree[check]:
            if el not in checked:
                to_check.append(el)
    return total - 1


def get_number_of_bags_contained(search_name: str, contains_dict: dict) -> int:
    to_check = [search_name]
    total = 0
    while to_check:
        check = to_check.pop()
        total += sum(contains_dict[check].values())
        for color, amount in contains_dict[check].items():
            to_check.extend([color] * amount)
        # to_check.extend(contains_dict[check].keys())
    return total


if __name__ == '__main__':
    lines = Path("input_files", "day7.txt").read_text().strip().splitlines()
    contains_dict = get_contains_dict(lines)
    print(contains_dict)
    print(f"Day 1: {can_contain_color_recursive('shiny gold', contains_dict)}")
    print(f"Day 2: {get_number_of_bags_contained('shiny gold', contains_dict)}")
