from collections import defaultdict
from typing import List

from tqdm import tqdm


def run_game(input_numbers: List[int], n_turns: int):
    d = defaultdict(list)
    for turn, number in enumerate(input_numbers, start=1):
        d[number].append(turn)
    last_number = input_numbers[-1]
    for turn in tqdm(range(len(input_numbers) + 1, n_turns + 1)):
        if len(d[last_number]) < 2:
            last_number = 0
        else:
            last_number = d[last_number][1] - d[last_number].pop(0)  # Pop since we only need to keep 2 numbers
        d[last_number].append(turn)
    return last_number


if __name__ == '__main__':
    print(f"Part one: {run_game([0, 13, 1, 8, 6, 15], 2020)}")
    print(f"Part two: {run_game([0, 13, 1, 8, 6, 15], 30000000)}")
