import re
import typing
from collections import Counter
from pathlib import Path


class PasswordWithPolicy(typing.NamedTuple):
    first_int: int
    second_int: int
    letter: str
    password: str


def is_valid_part_one(password: PasswordWithPolicy):
    c = Counter(password.password)
    return password.first_int <= c[password.letter] <= password.second_int


def xor(b1: bool, b2: bool):
    return b1 ^ b2


def is_valid_part_two(pw: PasswordWithPolicy):
    return xor(pw.password[pw.first_int - 1] == pw.letter, pw.password[pw.second_int - 1] == pw.letter)


def parse_lines(lines: typing.List[str]) -> typing.List[PasswordWithPolicy]:
    pattern = re.compile(r"(\d+)-(\d+) (\w): (\w+)")
    passwords = []
    for line in lines:
        match = pattern.match(line)
        passwords.append(PasswordWithPolicy(int(match.group(1)), int(match.group(2)), match.group(3), match.group(4)))
    return passwords


if __name__ == '__main__':
    lines = Path("input_files", "day2.txt").read_text().splitlines()
    lines = list(filter(len, lines))  # filter out empty lines
    passwords = parse_lines(lines)
    print("Answer 1:", sum(is_valid_part_one(pw) for pw in passwords))
    print("Answer 2:", sum(is_valid_part_two(pw) for pw in passwords))
