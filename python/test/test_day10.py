import pytest

from aoc2020.day10 import get_number_of_arrangements

_TEST_INPUT = """28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3"""

class TestGetNumberOfArrangements:
    def test_with_one_connection(self):
        numbers = [0, 3]
        result = get_number_of_arrangements(numbers)
        assert result == 1

    def test_with_one_connection_extended(self):
        numbers = [0, 3, 6, 9]
        result = get_number_of_arrangements(numbers)
        assert result == 1

    def test_with_no_connection(self):
        numbers = [0, 4]
        result = get_number_of_arrangements(numbers)
        assert result == 0

    def test_with_two_connections(self):
        numbers = [0, 1, 3]
        result = get_number_of_arrangements(numbers)
        assert result == 2

    def test_with_two_connections_extra_line(self):
        numbers = [0, 3, 4, 5]
        result = get_number_of_arrangements(numbers)
        assert result == 2

    def test_with_four_connections(self):
        numbers = [0, 1, 2, 3]
        result = get_number_of_arrangements(numbers)
        assert result == 4

    def test_aoc_test_input(self):
        numbers = list(map(int, _TEST_INPUT.splitlines()))
        numbers = [0, *numbers, max(numbers)]
        result = get_number_of_arrangements(numbers)
        assert result == 19208