import pytest

from aoc2020.day14 import mask_value, mask_memory_address


@pytest.mark.parametrize(("original_value", "expected_masked_value"), [
    (11, 73),
    (101, 101),
    (0, 64),
])
def test_mask_value(original_value: int, expected_masked_value: int):
    mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"

    masked = mask_value(original_value, mask)

    assert masked == expected_masked_value


def test_mask_memory_address_1():
    mask = "000000000000000000000000000000X1001X"
    original_value = 42

    masked = mask_memory_address(original_value, mask)

    assert masked == "000000000000000000000000000000X1101X"


def test_mask_memory_address_2():
    mask = "00000000000000000000000000000000X0XX"
    original_value = 26

    masked = mask_memory_address(original_value, mask)

    assert masked == "00000000000000000000000000000001X0XX"
