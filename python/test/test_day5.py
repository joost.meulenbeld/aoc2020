import pytest

from aoc2020.day5 import get_id_from_boarding_pass


@pytest.mark.parametrize(("boarding_pass", "bp_id"), [
    ("BFFFBBFRRR", 567),
    ("FFFBBBFRRR", 119),
    ("BBFFBBFRLL", 820),
])
def test_get_id(boarding_pass: str, bp_id: int):
    assert get_id_from_boarding_pass(boarding_pass) == bp_id
