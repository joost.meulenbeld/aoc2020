from typing import List

import pytest

from aoc2020.day15 import run_game


@pytest.mark.parametrize(("input_sequence", "expected"), [
    ([1, 3, 2], 1),
    ([2, 1, 3], 10)
])
def test_run_game(input_sequence: List[int], expected: int):
    result = run_game(input_sequence, 2020)

    assert result == expected
